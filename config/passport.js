const passport = require('passport')
  , BearerStrategy = require('passport-http-bearer').Strategy
  , sequelize = require('../models/sql');

passport.use(
  new BearerStrategy(
    (token, done)=>{
      sequelize.User.findOne({where: { token: token }})
        .then(user=>{
          if (!user) { return done(null, false); }
          return done(null, user, { scope: 'all' });
        })
        .catch(err=>{
          return done(err);
        })
    }
  )
);

passport.serializeUser(function(user, done) {
  console.log("Serialize User")
  done(null, user.id)
})

passport.deserializeUser(function(id, done) {
  console.log("DeSerialize User")
  Model.User.findOne({
    where: {
      'id': id
    }
  }).then(function (user) {
    if (user == null) {
      done(new Error('Wrong user id.'))
    }

    done(null, user)
  })
})

module.exports = passport