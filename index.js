const express = require('express')
const app = express()
const bodyParser = require('body-parser');
const compression = require("compression")
const cors = require('cors');
const session = require('express-session')
const config = require('./config/auth')
const logger = require('morgan')
// Init mysql ORM
require('./models/sql');
// Init mongoose ORM
require('./models/mongodb');
const passport = require('./config/passport')
app.use(passport.initialize())

const routes = require('./routes')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(compression());
app.use(session({
  secret: config.session.secret,
  resave: false,
  saveUninitialized: true,
  // cookie: { secure: true }
}))
app.use(logger('dev'))


var corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
  exposedHeaders: ['Content-Range', 'X-Content-Range', 'X-Total-Count'],
}
app.use(cors(corsOptions));

app.use('/', routes)

app.listen(3000, function () {
  console.log('POC API run on port 3000')
})