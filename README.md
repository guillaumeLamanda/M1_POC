# README

## Prérequis
- nodejs

## Lancer l'application

**A la racine du projet :**
- `npm install`
- `npm run dev`
Vous venez de lancer le serveur API.

**Dans le dossier client :**
- `npm run start`
  - Répondre "Y" quand le packager demande de lancer l'application sur un autre port.

## Communiquer avec l'API

Il est possible de tester les différentes fonctionnalités de l'API. Pour cela, il est conseiller d'utiliser [postman](https://www.getpostman.com).

Ensuite, importez la collection de requête présente à la racine du projet POC.postman_collection.json.
