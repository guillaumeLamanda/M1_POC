# Cahier des charges WeMoe - LAMANDA Guillaume - LECHAT Quentin

Ce document présente les fonctionnalités qui devront être présente dans le site de WeMoe. Il résumera aussi les informations manquantes pour qu'il puisse être complet.  

Rappel : WeMoe est une startup qui vise à simplifier l'information et l'accès au prêt aux personne présentant un risque aggravé de santé. Le site ne couvre que la partie information du projet.  

- [Cahier des charges WeMoe](#cahier-des-charges-wemoe)
  - [Analyse fonctionnelle](#analyse-fonctionnelle)
  - [Analyse technique](#analyse-technique)
    - [1. Articles](#1-articles)
    - [2. Inscription au site](#2-inscription-au-site)
    - [3. Personnalisation de l'expérience utilisateur](#3-personnalisation-de-lexp%C3%A9rience-utilisateur)
  - [Réalisation](#r%C3%A9alisation)
    - [1. Articles](#1-articles)
    - [2. Connexion/Inscription au site](#2-connexioninscription-au-site)
    - [3. Personnalisation de l'expérience utilisateur](#3-personnalisation-de-lexp%C3%A9rience-utilisateur)
  - [Données manquantes à la bonne réalisation du projet](#donn%C3%A9es-manquantes-%C3%A0-la-bonne-r%C3%A9alisation-du-projet)

## Analyse fonctionnelle
1. Le site permet à l'utilisateur un accès simplifié à l'information sous forme d'articles.  
2. L'utilisateur à la possibilité de s'inscrire au site.  
3. Quand l'utilisateur est connecté au site, les articles sont triés par pertinance.


## Analyse technique

### 1. Articles
Pour cette parties trois solutions techniques sont possibles : 
- La première solution (et pour moi la meilleure) est d'écrire les articles au format MarkDown. Voici les avantages et inconvéniants : 
  - A : Facile à écrire, modifier
  - A : Permet d'avoir un texte enrichi (tableau, image, titre, gras, italiques, liens ...)
  - I : Demande la prise en main du MarkDown (qui est relativement facile, voir [ici](https://fr.wikipedia.org/wiki/Markdown))
- La seconde est d'écrire les articles au format HTML 
  - A : Permet d'avoir un texte enrichi (tableau, image, titre, gras, italiques, liens ...)
  - I : Demande soit la mise en place d'une interface pour l'écriture des articles, soit une connaissance du HTML par l'autheur.
- La dernière est de stocker le texte seul en dur dans la base
  - A : Facile à mettre en place
  - I : L'article sera composé de texte simple seulement (pas de mise en forme, d'images ou tableaux)

La solution retenu pour la mise en place de ce POC est la troisième solution par contrainte de temps. Cependant la solution préconisée est la première.

### 2. Inscription au site
L'utilisateur a la possibilité de s'inscrire et/ou se connecter au site. 

Les informations suivantes les sont demandés : 
- nom
- prenom
- email
- password

De plus, une série de questions lui sont demandés afin de déterminé son profil (personne à risque, difficultée d'obtention d'un prêt).

### 3. Personnalisation de l'expérience utilisateur
Une fois inscrit et les informations renseignés, les articles proposés sont triés par ordre de pertinance. 

## Réalisation

D'un point de vue technique j'ai décidé de séparer le client et le serveur API.  
Le rôle de l'API est d'interfacer avec les données, et de donner l'application dynamique au client.  

L'API est écrite en nodeJS, et utilise le framework express.  
De plus elle implémente deux types de bases de données : mariadb et mongodb. Afin d'intéragir avec ces bases de données, on utilise un ORM pour chaque : Sequelize pour mariadb et Mongoose pour mongodb.  
Pour chaque model de la base de donnée (Articles, Users), on créer une route permettant d'interfacer avec ces données. Chaque route contient plusieurs méthodes : 
  - get pour récuperer la ressource,
  - post pour créer une ressource,
  - put pour modifier la ressource 
  - delete pour supprimer la ressource

![image sequelize](http://docs.sequelizejs.com/manual/asset/logo-small.png) <!-- .element height="250px" -->  
![image mongoose](https://www.js-tutorials.com/wp-content/uploads/2017/09/nodejs-mongodb-express-mongoose.png) <!-- .element height="250px" --> 
 
Le client quant à lui est écrit en ReactJS. Ceci nous permet d'avoir un site web dynamique. 
![reactjs](http://www.troispointzero.fr/wp-content/uploads/2016/03/reactjs.png) <!-- .element height="250px" --> 

### 1. Articles
L'article contiendra les champs suivants : 
  - titre
  - image
  - corps de texte
  - un tableau de tags (exemple : 'ras', 'coemprunteur', etc)

Du point de vue client, les articles sont présentées de la manières suivante : 
![articles](./docs/img/articles.png)

Une fois un article cliqué, il s'affiche dans un modal : 
![article_modal](./docs/img/modal_article.png)

Si l'utilisateur veut voir l'article dans une page à part, cela reste possible : 
![article](./docs/img/article.png)


### 2. Connexion/Inscription au site
L'inscription et la connexion au site se fait par un modal simple contenant un formulaire : 
![connexion](./docs/img/connexion.png)

Si l'utilisateur clique sur "inscription", alors le formulaire suivant s'affiche : 
![inscription1](./docs/img/inscription1.png)

Une fois cette première partie complété, la partie 2 s'affiche : 
![inscription2](./docs/img/inscription2.png)

Afin de garantir la sécurité de l'application, nous avons fait le choix de générer un token à la connexion ou l'inscription. Ainsi, les cas ou l'utilisateur doit entrer sont mot de passe sont limités. Ce token a une durée de validité de 2 semaines. 

### 3. Personnalisation de l'expérience utilisateur

Une fois que l'utilisateur a rempli son profile, on peut trier les articles afin que les articles les plus pertinents apparaissent en début de liste : 
![sorted_articles](./docs/img/sorted_articles.png)
Ici l'utilisateur à spécifié qu'il présentait un risque aggravé de santé. Les articles contenant le tag 'RAS' apparaissent donc d'abord.

## Données manquantes à la bonne réalisation du projet

- Liste des données a demander à l'utilisateur, afin d'en déduire les champs de bases de données à rajouter. Ceci nous permettrait aussi d'affiner les articles proposés à l'utilisateur,
- Plusieurs articles, un pour chaque "type" d'utilisateur que vous avez défini,
- La manière dont vous voulez administrer votre site et consulter les données
- Etablir une politique d'accès aux données pour chaque types d'utilisateurs (administrateur, utilisateur, etc...)
- Intégration d'un outil analytique pour 
  - Savoir quelles sont les pages les plus vues
  - Avoir un pourcentage de convertion (utilisateurs inscris / utilisateurs totaux)
  - Détecter les parties bloquantes (par exemple si il y a un grand nombre d'utilisateur commencant l'inscription mais qui ne la fini pas)
- Utiliser l'architecture mise en oeuvre au cours de ce projet
  - API centrique
  - Progressive web app
