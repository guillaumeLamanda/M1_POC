const express = require('express');
const router = express.Router();
const dbMysql = require('../../models/sql')
const mongoose = require('../../models/mongodb')
const passport = require('passport')

const needToBeAdmin = (req, res, next)=>{
  let isAdmin=req.user.accountType==='admin'
  let isThisUser = req.params.id && (parseInt(req.params.id)===parseInt(req.user.id))
  console.log(isAdmin, isThisUser)
  if(!isAdmin && !isThisUser) return res.json({message: "Non authorisé"})
  next()
}

router.route('/')
  .all(passport.authenticate('bearer'), 
    (req, res, next)=>{
      next()
  })
  .get((req,res)=>{
    if(req.user.accountType==='admin'){
      dbMysql.User.findAll().then(users=>{
        return res.json({
          users: users
        })
      })
    }
    else{
      return res.json(req.user)
    }
  })
  .post(needToBeAdmin, (req, res)=>{
    dbMysql.User.create(req.body).then(user=>{
      return res.json(user)
    })
    .catch(err=>res.status(400).json({message: "User ou email déjà enregistré"}))
  })

router.route('/:id')
  .all(passport.authenticate('bearer'))
  .delete(needToBeAdmin, (req, res)=>{
    dbMysql.User.destroy({where: 
      {
        id: req.params.id
      }
    }).then(isOk => res.json(isOk))
  })
  .put(needToBeAdmin, (req, res)=>{
    dbMysql.User.findById(req.params.id)
      .then(user=>{
        user.update(req.body)
          .then(updatedUser=>{
            return res.json(updatedUser)
          })
      })
      .catch(err=>{
        throw new Error(err);
      })
  })
  .get(needToBeAdmin, (req, res)=>{
    dbMysql.User.findById(req.params.id)
    .then(user=>{
      res.json(user)
    })
  })

router.get('/my', passport.authenticate('bearer'), (req, res)=>{
  res.json(req.user);
})

module.exports = router; 