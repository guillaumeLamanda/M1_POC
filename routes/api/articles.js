const express = require('express');
const router = express.Router();
const dbMysql = require('../../models/sql')
const mongoose = require('../../models/mongodb')

router.route('/')
  .get((req,res)=>{
    mongoose.model('Article').find().then(articles=>{
      return res.json({
        articles: articles
      })
    })
  })
  .post((req, res)=>{
    mongoose.model('Article').create(req.body).then(article=>{
      return res.json(article)
    })
  })

router.route('/:id')
  .get((req, res)=>{
    mongoose.model('Article').findById(req.params.id).then(article=>{
      res.json(article)
    })
  })
  .delete((req, res)=>{
    mongoose.model('Article').remove({where: 
      {
        id: req.params.id
      }
    }).then(isOk => res.json(isOk))
  })
  .put((req, res)=>{
    mongoose.model('Article').findByIdAndUpdate(req.params.id, {$set: req.body}, (err, article)=>{
      res.json(article);
    })
  })

module.exports = router; 