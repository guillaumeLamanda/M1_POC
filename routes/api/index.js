const express = require('express');
const router = express.Router();
const dbMysql = require('../../models/sql')
const mongoose = require('../../models/mongodb')

const users = require('./users')
const articles = require('./articles')
const connection = require('./connection')

router.use('/users', users);
router.use('/articles', articles);
router.use('/connection', connection);


router.route('/')
  .get((req, res)=>{
    return res.json({
      message: "Bienvenue sur notre API. Les routes disponibles sont :"
    })
  })

module.exports = router; 