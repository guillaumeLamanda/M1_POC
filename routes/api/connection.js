const express = require('express');
const router = express.Router();
const dbMysql = require('../../models/sql')
const mongoose = require('../../models/mongodb')
const Op = require('sequelize').Op;
const jwt = require('jsonwebtoken')
const config = require('../../config/auth')

router.route('/login')
  .post((req, res)=>{
    dbMysql.User.findOne({
      where: {
        [Op.or]: [ {username: req.body.login}, {email: req.body.login} ]
      }
    }).then(user=>{
      if(user.comparePassword(req.body.password)){
        let token = jwt.sign({user: user.id}, config.secret, {expiresIn: 60 * 60})
        user.token = token;
        user.save();
        return res.json(user)
      }
      else return res.json({message: "Connection refused"})
    })
    .catch(err=>{
      console.log(err)
      res.status(404).json({message: "User not found"})
    })
  })

router.post('/register', (req, res)=>{
  if((req.body.username || req.body.email) && req.body.password) {
    dbMysql.User.create(req.body)
      .then(user=>{
        let token = jwt.sign({user: user.id}, config.secret, {expiresIn: 60 * 60})
        user.token = token;
        user.save();
        res.json(user);
      })
      .catch(err=>res.status(500).json({message: "This username already exist"}))
  }
  else {
    res.status(400).json({message: "need a username/email and password"})
  }
})

module.exports = router; 