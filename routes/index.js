const express = require('express');
const router = express.Router();
const _ = require('lodash');
const fs = require('fs');
const { join, parse } = require('path')

/**
 * Router file. Define routes to use has middleware.
 * Last route is a redirect to kelvin. 
 * So every route controller must only make middleware treatment
 * and finish with next() function, for ending on kelvin.
 */

 /**
  * Functions
  */
// ---------------------------------------------
const isDirectory = source => fs.lstatSync(source).isDirectory()

const getDirectories = source =>
  fs.readdirSync(source).map(name => join(source, name)).filter(isDirectory)
// ---------------------------------------------

/**
 * Routes 
 */

router.get('/', (req,res)=>{
  res.json({
    message: "Bienvenue sur l'API du POC."
  })
})

/**
 * Create a route foreach directory in "/routes"
 */
getDirectories(__dirname).map(dir=>{
  let route=parse(dir).name
  router.use('/'+route, require(dir))
})

module.exports = router

