import React from 'react';
import PropTypes from 'prop-types';
import {Label, Button, Card, Image, Tab, Grid, Header, Divider, TransitionGroup } from 'semantic-ui-react'

import Login from './Login';
import Register from './Register';

class ConnectBox extends React.Component {
    state={
        subscribe: false
    }

    toogleSubscribe = () => {
        this.setState({subscribe: !this.state.subscribe})
    }

    render = () => {
        const { fLogin, fRegister, toogleDimmer, updateUser, user } = this.props;
        const { subscribe } = this.state;

        return(
            <Grid container centered>
                {
                    subscribe ?
                        <div>
                            <Register fRegister={fRegister} 
                                toogleDimmer={toogleDimmer}
                                updateUser={updateUser}
                                user={user}
                                loginText="Email"
                                passwordText="Mot de passe"
                                confirmText="Confirmez le mdp"
                            />
                        </div>
                    :
                        <div>
                            <Header textAlign='center' inverted content={"Connexion"} />
                            <Login fLogin={fLogin} 
                                toogleDimmer={toogleDimmer}
                                user={user}
                                loginText="Username ou email"
                                passwordText="Mot de passe"
                            />
                            <Divider inverted horizontal>OU</Divider>
                            <Grid.Row centered>
                                <Header textAlign='center' inverted content={"Pas encore inscrit ?"} />
                            </Grid.Row>
                            <Grid.Row centered>
                                <Button size='big' onClick={this.toogleSubscribe} primary content={"Inscription"} />
                            </Grid.Row>
                        </div>
                }
            </Grid>
        )
    }
}

ConnectBox.propTypes = {
    fLogin: PropTypes.func.isRequired,
    fRegister: PropTypes.func.isRequired
}

export default ConnectBox;