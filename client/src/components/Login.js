import React from 'react';
import { Container, Divider, Grid, Header, Image, Button, Input, Message } from 'semantic-ui-react'
import PropTypes from 'prop-types';

export default class Login extends React.Component {
    constructor(props){
        super(props)
        this.props=props;
        
        this.state={
            login:'',
            loginError: false,
            password:'',
            passwordError: false, 
            message: '',
            messageError: false
        }
    }

    _onChange = (event) => {
        let name = event.target.name,
            value = event.target.value;
        this.setState({[name]: value, loginError:false, passwordError:false, messageError: false});
    }

    _handleClick = () => {
        if(this.state.login==='' && this.state.password==='') return this.setState({loginError:true, passwordError:true});
        if(this.state.login===''){
            return this.setState({loginError:true});
        }
        if(this.state.password===''){
            return this.setState({passwordError:true});
        }
        this.props.fLogin(this.state.login, this.state.password)
        .then(()=>this.props.toogleDimmer())
        .catch(err=>{
            this.setState({message: err.message, messageError: true})
        })
    }

    render = () => {
        return (
            <Grid style={{ margin:0, justifyContent:'center'}}>
                <Grid.Row>
                    <Input inverted name="login" placeholder={this.props.loginText} 
                        type='email' icon={{name:'at'}} value={this.state.login} 
                        error={this.state.loginError}
                        onChange={this._onChange}/>
                </Grid.Row>
                <Grid.Row>
                    <Input name="password" placeholder={this.props.passwordText || "password" }
                        type='password' icon={{name:'genderless'}} value={this.state.password} 
                        error={this.state.passwordError}
                        onChange={this._onChange} />
                </Grid.Row>
                <Message error content={this.state.message} hidden={!this.state.messageError} />
                <Grid.Row>
                    <Button size='big' onClick={this._handleClick} content="Connexion" icon='rocket' basic color='yellow' />
                </Grid.Row>
            </Grid>
        );
    }
}

Login.propTypes={
    fLogin: PropTypes.func,
    loginText: PropTypes.string,
}

Login.defaultProps = {
    loginText: 'login | email'
}