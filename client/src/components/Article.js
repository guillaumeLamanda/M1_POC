import React from 'react'
import { 
    Container, 
    Segment, 
    Image, 
    Header, 
    Loader, 
    Button, 
    Icon,
    Grid
} from 'semantic-ui-react'
import { connect } from 'react-redux';
import { find } from 'lodash'
import { getArticle } from '../services/articles'

class ArticleContainer extends React.Component {
    state={
        article: null
    }

    componentWillMount = () => {
        const {match} = this.props
        const articleId = match.params.articleId
        let article ;
    
        getArticle(articleId).then(a=>{
            this.setState({article: a})
        })
    }

    render = () => {
        const {article} = this.state
        if(!article) return <Loader active />
        else return <Article article={article} goBack={this.props.history.goBack} />
    }

}

const Article = ({article, goBack}) =>  {
    return (
        <Grid container >
            <Grid.Row>
                <Button basic color="purple" circular animated="vertical" onClick={()=>goBack()}> 
                    <Button.Content hidden>Retour</Button.Content>
                    <Button.Content visible>
                    <Icon name="arrow left" size="large" fitted color="purple"/>
                    </Button.Content>
                </Button>
            </Grid.Row>
            <Grid.Row>
                <Container text>
                    {
                    article.image ? 
                        <Image src={article.image} />
                    : null
                    }
                    <Header as='h2'>{article.title}</Header>
                    <p>{article.content}</p>
                </Container>
            </Grid.Row>
        </Grid>
    )
}
  
  Article.propTypes= {
  }
  
  export default connect(state=>({
      articles: state.articles
  }))(ArticleContainer)