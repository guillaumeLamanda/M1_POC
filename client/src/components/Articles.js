import React from 'react';
import { PropTypes } from "prop-types";
import { connect } from 'react-redux';
import Segment from 'semantic-ui-react/dist/commonjs/elements/Segment/Segment';
import Header from 'semantic-ui-react/dist/commonjs/elements/Header/Header';
import Card from 'semantic-ui-react/dist/commonjs/views/Card/Card';
import Search from 'semantic-ui-react/dist/commonjs/modules/Search/Search';
import Divider from 'semantic-ui-react/dist/commonjs/elements/Divider/Divider';
import Dimmer from 'semantic-ui-react/dist/commonjs/modules/Dimmer/Dimmer';
import { Icon, Button, Label, Container, Image, Modal } from 'semantic-ui-react';

import {withRouter} from 'react-router-dom'
import {filter, escapeRegExp} from 'lodash'
import {getArticles} from '../services/articles'

import { sortBy } from 'lodash'

const possibleTags = [
  {
    text: "ras",
    icon: 'heart'
  },
  {
    text: "coemprunteur",
    icon: 'group'
  }
]

const getExtra = (article) => (
  <div>
    {
      possibleTags.map((tag, i)=>(
        article.tags.indexOf(tag.text)>0 ? <Label key={i} color='pink' icon={tag.icon} content={tag.text} /> : null
      ))
    }
  </div>
)

class Articles extends React.Component {
  state = {
    isLoading: false,
    results: this.props.articles,
    value: "",
    modalOpen: false
  }

  componentWillMount() {
    getArticles()
    this.resetComponent()
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.articles !== this.state.results) {
      this.setState({results: nextProps.articles})
    }
  }

  resetComponent = () => this.setState({ isLoading: false, results: this.props.articles, value: '' })

  handleSearchChange = (e, { value }) => {
    this.setState({ isLoading: true, value })

    setTimeout(() => {
      if (this.state.value.length < 1) return this.resetComponent()

      const re = new RegExp(escapeRegExp(this.state.value), 'i')
      const isMatch = result => re.test(result.title)

      this.setState({
        isLoading: false,
        results: filter(this.props.articles, isMatch),
      })
    }, 500)
  }

  render() {
    const {isLoading, results, value, article, modalOpen } = this.state ;
    const { user } = this.props;
    let articles = results
    console.log("user valide", user.valide)
    if( user.valide!==undefined && user.valide===false ) {
      articles = sortBy(results, (article)=>{ return article.tags.indexOf('ras')<0})
    }
    if( user.coemprunteur!==undefined && user.coemprunteur===true ) {
      articles = sortBy(results, (article)=>{ return article.tags.indexOf('coemprunteur')<0})
    }

    return (
      <Container>
        <Header as='h2' content="ARTICLES" />
        <Search 
          icon='search'
          value={value}
          loading={isLoading}
          onSearchChange={this.handleSearchChange}
          showNoResults={false}
        />
        <Divider />
        <Card.Group itemsPerRow={4} size='large' >
          {
            articles.map((article, i)=>(
              <Card image={article.image}
                key={i}
                header={article.title}
                description={article.top}
                extra={getExtra(article)}
                onClick={()=>this.setState({article: article, modalOpen: true})}
              />
            ))
          }
        </Card.Group>

        {
          article ? 
          <Modal open={modalOpen} onClose={()=>this.setState({modalOpen: false})} >
            <Modal.Content image scrolling >
              {
                article.image ? 
                  <Image size='medium' src={article.image} wrapped/>
                : null
              }
              <Modal.Description>
                <Header content={article.title} />
                <p style={{fontSize: '1.3em'}} >{article.content}</p>
              </Modal.Description>
            </Modal.Content>
            <Modal.Actions  >
              <Button primary onClick={()=>this.props.history.push('/articles/'+article._id)} >
                Ouvrir dans une nouvelle page <Icon  name='right chevron' />
              </Button>
            </Modal.Actions>
          </Modal>
          : null
        }
      </Container>
    )
  }
}

Articles.propTypes= {
  articles: PropTypes.array,
  history: PropTypes.object.isRequired
}

export default withRouter(connect(state=>({
  articles: state.articles,
  user: state.user
}))(Articles));