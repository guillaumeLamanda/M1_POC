import React from 'react';
import Company from './Company'
import Articles from './Articles'
import Divider from 'semantic-ui-react/dist/commonjs/elements/Divider/Divider';

const Home = () => (
  <div>
    <Company />
    <Divider />
    <Articles />
  </div>
)

export default Home ;