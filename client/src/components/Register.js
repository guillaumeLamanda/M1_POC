import React from 'react';
import { PropTypes } from "prop-types";
import { Loader, Segment, Container, Divider, Grid, Message, Image, Button, Checkbox, Input } from 'semantic-ui-react'

class Register extends React.Component {
    state = {
        part: 1
    }

    _changePart = (number) => {
        this.setState({part: number})
    }


    
    render(){
        switch (this.state.part) {
            case 1:
                return <Part1 {...this.props} changePart={this._changePart} />
            case 2: 
                return <Part2 {...this.props} changePart={this._changePart} />
            default:
                return <Loader active />
        }
    }
}

export default Register

class Part1 extends React.Component {
    state={
        login:'',
        password:'',
        confirm:'',
        message:'',
        newsletter: true,
        loginError: false,
        passwordError: false,
        confirmError: false,
        messageError: false
    }

    _onChange = (event) => {
        let name = event.target.name,
            value = event.target.value;
        // console.log('before change ', name, this.state[name], value);
        this.setState({[name]: value, messageError: false});
    }

    _handleClick = () => {
        console.log("handle click")
        if(this.state.login==='' && this.state.password==='') return this.setState({loginError:true, passwordError:true});
        else if(this.state.login===''){
            return this.setState({loginError:true});
        }
        else if(this.state.password===''){
            return this.setState({passwordError:true});
        }
        else if(this.state.password!==this.state.confirm) this.setState({passwordError: true, confirmError: true})
        else {
            this.props.fRegister(this.state.login, this.state.password)
            .then(()=>{
                console.log("ici")
                this.props.changePart(2);
            })
            .catch(err=>{
                this.setState({message: err.message, messageError: true})
            })
        }
    }

    render = () => {
        const { loginText, passwordText, confirmText } = this.props
        const { login, password, confirm, loginError, passwordError, confirmError, message, messageError, newsletter } = this.state ;
        return (
            <Grid container >
                <Grid.Row centered >
                    <Input name="login" 
                        placeholder={loginText || "Email"} 
                        type='email'
                        icon={{name:'at'}} 
                        value={login} 
                        onChange={this._onChange}
                        error={loginError}
                    />
                </Grid.Row>
                <Grid.Row centered>
                    <Input name="password" 
                        placeholder={passwordText || "password"} 
                        type='password' 
                        icon={{name:'genderless'}} 
                        value={password} 
                        onChange={this._onChange} 
                        error={passwordError}
                    />
                </Grid.Row>
                <Grid.Row centered>
                    <Input name="confirm" 
                        placeholder={confirmText || "confirm" }
                        type='password' 
                        icon={{name:'genderless'}} 
                        value={confirm} 
                        onChange={this._onChange} 
                        error={confirmError}
                    />
                </Grid.Row>
                <Grid.Row centered>
                    <Segment>
                        <Checkbox label="S'inscrire à la newsletter" 
                            onClick={()=>{this.setState({newsletter: !newsletter})}} 
                            checked={newsletter}
                        /> 
                    </Segment>
                </Grid.Row>
                <Message error content={message} hidden={!messageError} />
                <Grid.Row centered>
                    <Button size='big' icon='rocket' basic color='violet' content="S'enregistrer" onClick={this._handleClick} />
                </Grid.Row>
            </Grid>
        );
    }
}

Part1.propTypes={
    fRegister: PropTypes.func.isRequired,
    loginText: PropTypes.string,
    passwordText: PropTypes.string,
    confirmText: PropTypes.string,
}

class Part2 extends React.Component {
    constructor(props){
        super(props)
        this.props=props;
        
        this.state={
            username: '',
            nom:'',
            prenom:'',
            email:'',
            age:0,
            valide: true,
            coemprunteur: false,
            message: '',
            messageError: false
        }
    }

    _onChange = (event) => {
        let name = event.target.name,
            value = event.target.value;
        this.setState({[name]: value, messageError: false});
    }

    _handleClick = () => {
        const {message, messageError, ...userInfo} = this.state
        this.props.updateUser(userInfo, this.props.user.id)
        .then(()=>{
            this.props.toogleDimmer()
        })
        .catch(err=>{
            this.setState({message: err.message, messageError: true})
        })
    }

    render() {
        const { username, nom, prenom, email, age, valide, coemprunteur, message, messageError } = this.state ;
        return (
            <Grid centered >
                <Grid.Row centered>
                    <Message header={"Pourquoi ces informations ?"}
                        content={"Elles permettent de sélectionner nos articles les plus adapté à vos besoins"}
                        icon='question'
                        info
                    />
                </Grid.Row>
                <Grid.Row centered >
                    <Input name="username" 
                        placeholder={"nom d'utilisateur"} 
                        value={username} 
                        onChange={this._onChange}
                    />
                </Grid.Row>
                <Grid.Row>
                    <Input name="nom" 
                        placeholder={"nom"} 
                        value={nom} 
                        onChange={this._onChange}
                    />
                </Grid.Row>
                <Grid.Row>
                    <Input name="prenom" 
                        placeholder={"prenom"} 
                        value={prenom} 
                        onChange={this._onChange}
                    />
                </Grid.Row>
                <Grid.Row>
                    <Input name="age" 
                        placeholder={"Age"} 
                        type='number'
                        value={prenom} 
                        onChange={this._onChange}
                    />
                </Grid.Row>
                <Grid.Row>
                    <Segment>
                        <Checkbox
                            toggle
                            onClick={()=>{this.setState({valide:!valide})}}
                            value={!valide}
                            label={"Je présente un risque de santé"}
                        />
                    </Segment>
                </Grid.Row>
                <Grid.Row>
                    <Segment>
                        <Checkbox
                            toggle
                            onClick={()=>{this.setState({coemprunteur:!coemprunteur})}}
                            value={coemprunteur}
                            label={"coemprunteur"}
                        />
                    </Segment>
                </Grid.Row>
                <Grid.Row centered>
                    <Button positive size='big' icon='checkmark' content="OK" onClick={this._handleClick} />
                </Grid.Row>
            </Grid>
        );
    }
}