import React from 'react';
import { PropTypes } from "prop-types";
import { Dropdown, Menu, Image, Modal } from 'semantic-ui-react';
import { connect } from 'react-redux'
import Header from 'semantic-ui-react/dist/commonjs/elements/Header/Header';
import Icon from 'semantic-ui-react/dist/commonjs/elements/Icon/Icon';
import ConnectBox from './ConnectBox'

import { login, register, updateUser, disconnect } from '../services/users/'
class Nav extends React.Component {
  state = {
    loginOpen:false
  }

  toogleDimmer = () => {
    this.setState({loginOpen: !this.state.loginOpen})
  }

  render(){
    const { user } = this.props;

    const { loginOpen } = this.state;
    return (
      <div>
        <Menu style={{marginBottom: '2rem'}} fluid>
          <Menu.Item header>
            <Header as='h1' color='pink' content='WeMoe' subheader="Promoteur d'assurance collective"/>
          </Menu.Item>
          <Modal size='small' open={loginOpen} onClose={this.toogleDimmer} basic> 
              <Modal.Content>
                <ConnectBox user={user} 
                  toogleDimmer={this.toogleDimmer} 
                  fLogin={login} 
                  fRegister={register} 
                  updateUser={updateUser}
                />
              </Modal.Content>
          </Modal>
    
          {
              this.props.user.token===undefined ? 
                  <Menu.Item position='right'onClick={this.toogleDimmer}>
                    <Icon name='user' size='big' color='purple' />
                    Se Connecter
                  </Menu.Item>
              :
              <Menu.Item position='right'>
                <Dropdown pointing='left' pointing={false} trigger={<UserAvatar user={user} />} >
                  <Dropdown.Menu>
                    <Dropdown.Item text='Déconnexion' onClick={()=>disconnect()} />
                  </Dropdown.Menu>
                </Dropdown>
              </Menu.Item>
          }
        </Menu>
      </div>
    )
  }
}

Nav.propTypes = {
  user: PropTypes.object,
}

const UserAvatar = ({user}) => (
  <Image centered avatar shape='circular' verticalAlign='middle' src={user.img} />
)
  

export default connect(state=>({
  user: state.user
}))(Nav);