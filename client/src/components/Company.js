import React from 'react';
import { PropTypes } from "prop-types";

import Segment from 'semantic-ui-react/dist/commonjs/elements/Segment/Segment';
import Header from 'semantic-ui-react/dist/commonjs/elements/Header/Header';
import Grid from 'semantic-ui-react/dist/commonjs/collections/Grid/Grid';
import Icon from 'semantic-ui-react/dist/commonjs/elements/Icon/Icon';
import Divider from 'semantic-ui-react/dist/commonjs/elements/Divider/Divider';
import Card from 'semantic-ui-react/dist/commonjs/views/Card/Card';
import List from 'semantic-ui-react/dist/commonjs/elements/List/List';
import Container from 'semantic-ui-react/dist/commonjs/elements/Container/Container';

const content = {
  presentations: {
    title: "NOTRE BUT",
    items: [
      {
        icon: 'book',
        header: "Documenter",
        subheader: "Simplifier la compréhension des assurances"
      },
      {
        icon: 'users',
        header: "Assurer collectivement",
        subheader: "Simplifier d'accès à l'assurance"
      }
    ]
  },
  choicesTitle: "Nous somme utile si : ",
  choices: [
    {
      title: "Je veux faire un empreint et j'ai besoin d'informations",
      items: [
        "Contenu informationnel",
        "Inscription à la liste de diffusion pour être informé des activités de we moë",
        "Accès au contrat collectif proposé par we moë"
      ]
    },
    {
      title: "Je veux changer d'assurance",
      items: [
        "Contenu informationnel",
        "Inscription à la liste de diffusion pour être informé des activités de we moë",
        "Accès au contrat collectif proposé par we moë"
      ]
    }
  ]
}

const Company = () => (
  <Container>
    <Header as='h2' content={content.presentations.title} />
    <Grid columns={content.presentations.items.length} >
      {
        content.presentations.items.map((item, i)=>(
          <Grid.Column key={i} >
            <Header icon >
              <Icon name={item.icon} />
              {item.header}
              <Header.Subheader>{item.subheader}</Header.Subheader>
            </Header>
          </Grid.Column>
        ))
      }
    </Grid>
    <Header as='h3' content={content.choicesTitle} /> 
    <Grid columns={content.choices.length}>
      {
        content.choices.map((choice, i)=>(
          <Grid.Column key={i} >
            <Card fluid >
              <Card.Content header={choice.title} />
              <Card.Content>
                <Card.Description textAlign='left' >
                  <List as='ul'>
                    {
                      choice.items.map((item, j)=>(
                        <List.Item key={j} as='li'>{item}</List.Item>
                      ))
                    }
                  </List>
                </Card.Description>
              </Card.Content>
            </Card>
          </Grid.Column>

        ))
      }
      <Grid.Column>

      </Grid.Column>
    </Grid>
  </Container>
)

Company.propTypes= {

}

export default Company;