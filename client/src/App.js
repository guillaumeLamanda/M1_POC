import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import {Provider} from 'react-redux'
import store from './store'

import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'

import Home from './components/Home'
import Nav from './components/Nav'
import Articles from './components/Articles'
import Article from './components/Article'

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <Router>
            <div>
              <Nav />
              <Route exact path='/' component={Home} />
              <Route exact path="/articles" component={Articles} />
              <Route path='/articles/:articleId' component={Article} /> 
            </div>
          </Router>
        </div>
      </Provider>
    );
  }
}

export default App;
