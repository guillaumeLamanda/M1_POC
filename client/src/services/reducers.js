import {combineReducers } from 'redux';

import {reducer as articlesReducer } from './articles/reducer';
import {reducer as usersReducer } from './users/reducer';

export const reducer = combineReducers({
  user: usersReducer,
  articles: articlesReducer
});
