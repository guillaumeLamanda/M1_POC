import api from '../api'
import store from '../../store'
import { setArticles } from './reducer'

const endPoints = {
  articles: '/articles',
  article: (id)=>'articles/'+id
}

export const getArticles = () => api.get(endPoints.articles)
  .then(response=>{
    store.dispatch(setArticles(response.data.articles))
  })

export const getArticle = (id) => {
  return api.get(endPoints.article(id))
    .then(res=>{
      if(res.ok) return res.data
      else return null
    })
}
