import store from '../../store'

/**
 * Actions Types
 */
const SET_ARTICLES = 'articles/SET_ARTICLES';

/**
 * actions
 */
export const setArticles = (articles) => ({
	type: SET_ARTICLES,
  articles: articles
});

/**
 * Initial state
 */
// const initialState = {
//   username: undefined,
//   nom: undefined,
//   prenom: undefined,
//   email: undefined,
//   age: undefined,
//   sexe: undefined,
//   valide: undefined,
//   coemprunteur: undefined
// }
const initialState = []
/**
 * reducer
 */
export const reducer = (state = initialState, action) => {
	switch (action.type) {
    case SET_ARTICLES:
      return action.articles;
    default: 
      return state;
  }
}