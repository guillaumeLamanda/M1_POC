import { create } from 'apisauce';

const api = create({
  baseURL: '/api/',
  headers: {
    Authorization: 'bearer '+localStorage.getItem('token')
  }
})

export default api