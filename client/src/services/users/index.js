import api from '../api'
import store from '../../store'
import { 
  setUser, 
  updateUser as rUpdateUser,
  disconnect as rDisconnect
} from './reducer'
import gravatar from 'gravatar'

const endPoints = {
  users: '/users',
  login: '/connection/login',
  register: '/connection/register',
  my: '/users/my',
  update: (id)=> '/users/'+id
}

export const login = (login, password) =>{
  return api.post(endPoints.login, {login, password})
    .then(response=>{
      if(!response.ok) throw new Error(response.data.message)
      api.setHeader('Authorization', 'bearer '+response.data.token)
      response.data.img = gravatar.url(response.data.email, {s: '200', f: 'y', d: 'mm', protocol: 'http'})
      return store.dispatch(setUser(response.data))
    })
}

export const register = (email, password) => {
  return api.post(endPoints.register, {email, password})
    .then(response=>{
      if(!response.ok) throw new Error(response.data.message)
      api.setHeader('Authorization', 'bearer '+response.data.token)
      let user = response.data
      user.img = gravatar.url(user.email, {s: '200', f: 'y', d: 'mm', protocol: 'http'})
      if(response.ok) store.dispatch(setUser(user))
    })
}

export const updateUser = (infos, id) => {
  return api.put(endPoints.update(id), infos)
    .then(response=>{
      if(!response.ok) throw new Error(response.data.message)
      if(response.ok) store.dispatch(rUpdateUser(response.data))
    })
}

export const disconnect = () => {
  return store.dispatch(rDisconnect())
}

const token = localStorage.getItem('token');
console.log(token)
if(token && token!==undefined){
  api
    .get(endPoints.my)
    .then(response=>{
      const user = response.ok ? response.data : {}
      if(!user.img && response.ok) user.img = gravatar.url(user.email, {s: '200', f: 'y', d: 'mm', protocol: 'http'})
      store.dispatch(setUser(user))
    })
    .catch(()=>localStorage.removeItem('token'))
}
else{
  localStorage.removeItem('token')
}