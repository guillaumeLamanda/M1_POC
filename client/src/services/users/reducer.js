import store from '../../store'

/**
 * Actions Types
 */
const SET_USER = 'user/SET_USER';
const UPDATE_USER = 'user/UPDATE_USER';
const DISCONNECT = 'user/DISCONNECT';

/**
 * actions
 */
export const setUser = (user) => ({
	type: SET_USER,
  user: user
});
export const updateUser = (user) => ({
	type: UPDATE_USER,
  user: user
});

export const disconnect= () => ({
  type: DISCONNECT
})

/**
 * Initial state
 */
const initialState = {
  username: undefined,
  nom: undefined,
  prenom: undefined,
  email: undefined,
  age: undefined,
  sexe: undefined,
  valide: undefined,
  coemprunteur: undefined,
  token: undefined
}
/**
 * reducer
 */
export const reducer = (state = initialState, action) => {
	switch (action.type) {
    case SET_USER:
      localStorage.setItem('token', action.user.token)
      return action.user;
    case UPDATE_USER:
      return Object.assign({}, state, action.user);
    case DISCONNECT:
      localStorage.removeItem('token')
      return initialState;
    default: 
      return state;
  }
}