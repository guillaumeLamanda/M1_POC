import { createStore, combineReducers,applyMiddleware, compose } from 'redux';
import {persistStore} from 'redux-persist'
import { createFilter, createBlacklistFilter } from 'redux-persist-transform-filter';

import {reducer} from './services/reducers';

import thunk from 'redux-thunk';

import devTools from 'remote-redux-devtools';

const enhancer = compose(
	applyMiddleware(
		thunk,
	),
	devTools()
);

const store = createStore(
	reducer,
	enhancer,
);

persistStore(store)

export default store;
