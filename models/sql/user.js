const bcrypt = require('bcrypt')

module.exports = (sequelize, DataTypes) => {
  var User = sequelize.define('User', {
    username: {
      type: DataTypes.STRING,
      unique: true
    },
    nom: DataTypes.STRING,
    prenom: DataTypes.STRING,
    email: DataTypes.STRING,
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    token: DataTypes.STRING,
    age: DataTypes.INTEGER,
    sexe: DataTypes.ENUM('H','F'),
    valide: DataTypes.BOOLEAN,
    coemprunteur: DataTypes.BOOLEAN,
    accountType: {
      type: DataTypes.ENUM('admin','user','assurance','banque','writter'),
      allowNull: false,
      defaultValue: 'user'
    },
    img: DataTypes.STRING,
    newsletter: DataTypes.BOOLEAN
  });

  User.beforeCreate(user=>{
    user.password = bcrypt.hashSync(user.password, 10);        
  })

  User.beforeUpdate((user, options)=>{
    if(user._changed.password && user._changed.password===true){
        user.password = bcrypt.hashSync(user.password, 10);
    }
  })

  User.prototype.toJSON =  function () {
    var values = Object.assign({}, this.get());
  
    delete values.password;
    return values;
  }

  User.prototype.comparePassword = function(passwd){
    if(!this.password) throw new Error("User doesn't have password");
    var isMatch=bcrypt.compareSync(passwd, this.password);
    return isMatch;
  }

  return User;
};