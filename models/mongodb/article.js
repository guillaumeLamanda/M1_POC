const mongoose = require('mongoose');

module.exports = mongoose.model('Article', {
  title: String,
  top: String,
  content: String,
  image: String,
  tags: Array.of(String),
})